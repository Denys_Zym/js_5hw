//Теория
// 1 Опишіть своїми словами, що таке метод об'єкту
// Метод это функция внутри объекта. что бы сослаться на свойство в объекте нужно указать this. (или object.)

// 2 Який тип даних може мати значення властивості об'єкта?
// В объекте может храниться любой тип данных

// 3 Об'єкт це посилальний тип даних. Що означає це поняття?
// Объект хранит не данные, а ссылки на данные и копирование происходит по ссылке.

// Практика
// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.

let first = prompt("Input first name");
let last = prompt("Input last name");

function createNewUser(name1, name2) {
  let newUser = {
    FirstName: name1,
    LastName: name2,
    getLogin() {
      return (
        this.FirstName.toLowerCase().slice(0, 1) + this.LastName.toLowerCase()
      );
    },
  };
  return newUser.getLogin();
}
console.log(createNewUser(first, last));
alert(createNewUser(first, last));

//-----------------------------------------------------------------------------------
// function User(fullName) {
//   this.fullName = fullName;

//   Object.defineProperties(this, {
//     firstName: {
//       get: function () {
//         return this.fullName.split(" ")[0];
//       },

//       set: function (newFirstName) {
//         this.fullName = newFirstName + " " + this.lastName;
//       },
//     },

//     lastName: {
//       get: function () {
//         return this.fullName.split(" ")[1];
//       },

//       set: function (newLastName) {
//         this.fullName = this.firstName + " " + newLastName;
//       },
//     },
//   });
// }
// var vasya = new User("Василий Попкин");

// alert(vasya.fullName);

// setFirstName(newFirstName) {
//   this.setFirstName = newFirstName;
//   return;
// },
// setLastName(newLastName) {
//   this.setLastName = newLastName;
//   return;
// },
